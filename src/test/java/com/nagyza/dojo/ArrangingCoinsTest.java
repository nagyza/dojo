package com.nagyza.dojo;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class ArrangingCoinsTest {

    private ArrangingCoins underTest;

    @Before
    public void setUp() {
        underTest = new ArrangingCoins();
    }

    @Test
    public void shouldReturnExpectedRowsZeroZero() {
        int coins = 0;
        int expectedRows = 0;
        int actualRows = underTest.arrangeCoins(coins);
        assertEquals(actualRows, expectedRows);
    }

    @Test
    public void shouldReturnExpectedRowsOneOne() {
        int coins = 1;
        int expectedRows = 1;
        System.out.println(coins + " - " + expectedRows);
        int actualRows = underTest.arrangeCoins(coins);
        System.out.println(actualRows + " -----");
        assertEquals(expectedRows, actualRows);
    }

    @Test
    public void shouldReturnExpectedRowsTwoOne() {
        int coins = 2;
        int expectedRows = 1;
        int actualRows = underTest.arrangeCoins(coins);
        assertEquals(expectedRows, actualRows);
    }

    @Test
    public void shouldReturnExpectedRowsThreeTwo() {
        int coins = 3;
        int expectedRows = 2;
        int actualRows = underTest.arrangeCoins(coins);
        assertEquals(actualRows, expectedRows);
    }

    @Test
    public void shouldReturnExpectedRowsFourTwo() {
        int coins = 4;
        int expectedRows = 2;
        int actualRows = underTest.arrangeCoins(coins);
        assertEquals(actualRows, expectedRows);
    }

    @Test
    public void shouldReturnExpectedRowsFiveTwo() {
        int coins = 5;
        int expectedRows = 2;
        int actualRows = underTest.arrangeCoins(coins);
        assertEquals(actualRows, expectedRows);
    }

    @Test
    public void shouldReturnExpectedRowsSixThree() {
        int coins = 6;
        int expectedRows = 3;
        int actualRows = underTest.arrangeCoins(coins);
        assertEquals(actualRows, expectedRows);
    }

    @Test
    public void shouldReturnExpectedRowsSevenThree() {
        int coins = 7;
        int expectedRows = 3;
        int actualRows = underTest.arrangeCoins(coins);
        assertEquals(actualRows, expectedRows);
    }

    @Test
    public void shouldReturnExpectedRowsEightThree() {
        int coins = 8;
        int expectedRows = 3;
        int actualRows = underTest.arrangeCoins(coins);
        assertEquals(actualRows, expectedRows);
    }

    @Test
    public void shouldReturnExpectedRowsNineThree() {
        int coins = 9;
        int expectedRows = 3;
        int actualRows = underTest.arrangeCoins(coins);
        assertEquals(actualRows, expectedRows);
    }

    @Test
    public void shouldReturnExpectedRowsTenFour() {
        int coins = 10;
        int expectedRows = 4;
        int actualRows = underTest.arrangeCoins(coins);
        assertEquals(actualRows, expectedRows);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenInputIsNull() {
        Integer coins = null;
        underTest.arrangeCoins(coins);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenInputIsNegative() {
        Integer coins = -1;
        underTest.arrangeCoins(coins);
    }

    @Test
    public void shouldReturnExpectedResult() {
        int actualRows = underTest.arrangeCoins(20);
        assertEquals(actualRows, 5);
    }
}
