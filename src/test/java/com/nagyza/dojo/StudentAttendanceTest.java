package com.nagyza.dojo;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class StudentAttendanceTest {

    private StudentAttendance underTest = new StudentAttendance();

    @Test
    public void shouldPassForGoodStudent() {
        //GIVEN

        //WHEN
        boolean result = underTest.checkRecord("PPALLP");

        //THEN
        assertTrue(result);
    }

    @Test
    public void shouldFailForSleepyStudent() {
        //GIVEN

        //WHEN
        boolean result = underTest.checkRecord("PPALLL");

        //THEN
        assertFalse(result);
    }

    @Test
    public void shouldFailForAbsentStudent() {
        //GIVEN

        //WHEN
        boolean result = underTest.checkRecord("AA");

        //THEN
        assertFalse(result);
    }

    @Test
    public void shouldPassForStudentLinvingOnTheEdge() {
        //GIVEN

        //WHEN
        boolean result = underTest.checkRecord("LALL");

        //THEN
        assertTrue(result);
    }
}
