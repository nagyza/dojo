package com.nagyza.dojo;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(JUnitParamsRunner.class)
public class DisappearedNumbersTest {

    private DisappearedNumbers underTest;

    @Before
    public void setUp() {
        underTest = new DisappearedNumbers();
    }

    private Object[][] testData() {
        return new Object[][] {
                { new int[]{2, 3, 4, 5, 5}, new int[]{1} },
                { new int[]{4, 3, 2, 7, 8, 2, 3, 1}, new int[]{5, 6} },
                { new int[]{1, 12, 2, 14, 13, 6, 7, 8, 9, 10, 1, 12, 4, 14, 15}, new int[]{3, 5, 11} },
                { new int[]{5, 2, 1, 4, 3}, new int[]{} },
                { new int[]{}, new int[]{} }
        };
    }

    private Object[][] negativeTestData() {
        return new Object[][] {
                { new int[]{2, 3, 4, 5, 5} },
                { new int[]{4, 3, 2, 7, 8, 2, 3, 1} },
                { new int[]{1, 12, 2, 14, 13, 6, 7, 8, 9, 10, 1, 12, 4, 14, 15} },
                { new int[]{5, 2, 1, 4, 3} }
        };
    }

    private Object[][] invalidInput() {
        return new Object[][] {
                { new int[]{1, 4} },
                { new int[]{5, 2, -1, 4, 3} }
        };
    }

    @Test
    @Parameters(method = "testData")
    public void shouldFindDisappearedNumbers(int[] numbers, int[] expectedNumbers) {
        List<Integer> disappearedNumbers = underTest.findDisappearedNumbers(numbers);
        assertResult(expectedNumbers, disappearedNumbers);
    }

    @Test
    @Parameters(method = "negativeTestData")
    public void shouldNotContainExistingNumber(int[] numbers) {
        List<Integer> disappearedNumbers = underTest.findDisappearedNumbers(numbers);
        List<Integer> numbersList = arrayToList(numbers);
        boolean hasOnlyMissingNumbers = disappearedNumbers.stream().noneMatch(numbersList::contains);
        assertTrue(hasOnlyMissingNumbers);
    }

    @Test(expected = IllegalArgumentException.class)
    @Parameters(method = "invalidInput")
    public void shouldThrowExceptionWhenInputContainsNumberNotInRange(int[] numbers) {
        underTest.findDisappearedNumbers(numbers);
    }

    private List<Integer> arrayToList(int[] expectedNumbers) {
        return IntStream.of(expectedNumbers).boxed().collect(Collectors.toList());
    }

    private void assertResult(int[] expectedNumbers, List<Integer> disappearedNumbers) {
        List<Integer> list = arrayToList(expectedNumbers);
        assertEquals(list.size(), disappearedNumbers.size());
        assertTrue(disappearedNumbers.containsAll(list));
    }
}
